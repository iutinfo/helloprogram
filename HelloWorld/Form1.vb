﻿Imports HelloWorldLib

Public Class Form1
    Private Sub Button1_MouseClick(sender As Object, e As MouseEventArgs) Handles Button1.MouseClick
        MessageBox.Show(GreetingsClass.HelloWorld())
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Label2.Text = (GreetingsClass.SayHello("Victor"))
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If TextBox1.Text.Length > 0 Then
            Label2.Text = (GreetingsClass.SayHello(TextBox1.Text))
        Else
            MessageBox.Show("Il faut entrer un nom !")
        End If

    End Sub
End Class
