﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HelloWorldLib;

namespace HelloWorldLibTest
{
    [TestClass]
    public class GreetingsClassTest
    {
        [TestMethod]
        public void HelloWorldTest()
        {
            Assert.AreEqual("Hello World !", GreetingsClass.HelloWorld());
        }

        [TestMethod]
        public void SayHelloTest()
        {
            Assert.AreEqual("Bonjour Thomas, comment ça va ?", GreetingsClass.SayHello("Thomas"));
            Assert.AreEqual("Bonjour Alexis, comment ça va ?", GreetingsClass.SayHello("Alexis"));
            Assert.AreEqual("Bonjour Baptiste, comment ça va ?", GreetingsClass.SayHello("Baptiste"));
        }
    }
}
